import { AppPage } from './app.po';
import { browser, by, element, ElementFinder } from 'protractor';

describe('tabs', () => {

  beforeEach(() => {
    browser.get('/');
  });

  it('The "Home" page is displayed by default ', () => {
    expect(element(by.css('.tab-selected ion-label')).getText()).toContain("Home");
  });

  it('the user can navigate to the "Add Product" tab', async () => {
    await element(by.css('[tab=tab2]')).click();
    browser.driver.sleep(500);
    expect(element(by.css('.tab-selected ion-label')).getText()).toContain("Add Product");
  });
});
