import { Component, OnInit } from '@angular/core';
import { Product } from "../product/product";
import { ProductService } from "../product.service";
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.page.html',
  styleUrls: ['./add-product.page.scss'],
})
export class AddProductPage implements OnInit {

  pid: string
  title : string
  id : string
  description : string
  price : number
  pictures : Array<any>
  date : number
  user_id : string
  image

    constructor(
      private productservice : ProductService,
      private imagePicker : ImagePicker,
      private webview: WebView,
      private camera: Camera,
      public router: Router
      ) {}
    
    ngOnInit() {
    }
  
    add_product() {
      let user = JSON.parse(localStorage.getItem('user'))
      if(!user) return false
      try {
        if(this.image) {
          this.productservice.uploadPicture(this.image).then(res => {
            this.add(res)
          })
        }else {
          this.add()
        }
      } catch (error) {
        alert(error.message)
      }
      
    }
  
    add(res:any = '') {
      const productdata : Product = {
        title : this.title,
        description : this.description,
        price : this.price,
        pictures : res,
        pid : '',
        id : '',
        date : 0,
        user_id : ''
      }
      this.productservice.add_product(productdata).then(res => {
        this.router.navigate(['/product-detail/'+res])
      })
      
    }

    takePhoto() {
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((image)=>{
        //'data:image/jpeg;base64,' +
        this.image = 'data:image/jpeg;base64,' + image
      })
    }

    openImagePicker(){
      let options = {
        width: 200,
        quality: 25,
        outputType: 1
      };
      console.log('hi')
      this.imagePicker.getPictures(options).then(
        (results) => {
          for (var i = 0; i < results.length; i++) {
            //this.uploadImageToFirebase(results[i]);
          }
        }, (err) => console.log(err)
      );
    }

      uploadImageToFirebase(image){
        image = this.webview.convertFileSrc(image);
      
        //uploads img to firebase storage
        this.productservice.uploadFile(image)
        /*.then(photoURL => {
      
          let toast = this.toastCtrl.create({
            message: 'Image was updated successfully',
            duration: 3000
          });
          toast.present();
          })*/
        }

  }
