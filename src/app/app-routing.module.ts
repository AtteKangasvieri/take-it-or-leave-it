import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'register',
    redirectTo: '/tabs/register'
  },
  {
    path: 'login',
    redirectTo: '/tabs/login'  
  },
  {
    path: 'privacy-policy',
    redirectTo: '/tabs/privacy-policy'  
  },
  {
    path: 'terms-of-use',
    redirectTo: '/tabs/terms-of-use'  
  },
  {
    path: 'search',
    redirectTo: '/tabs/search'  
  },
  {
    path: 'add-product',
    redirectTo: '/tabs/add-product'  
  },
  {
    path: 'edit-product',
    redirectTo: '/tabs/edit-product'  
  },
  {
    path: 'image-picker',
    redirectTo: '/tabs/image-picker'  
  },
  {
    path: 'edit-profile',
    redirectTo: '/tabs/edit-profile'  
  },
  {
    path: 'product-detail/:id',
    redirectTo: '/tabs/product-detail/:id'  
  }




];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
