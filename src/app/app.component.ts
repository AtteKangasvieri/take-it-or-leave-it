import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AuthenticationService } from "./authentication.service";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  navigate : any;

  constructor
    (private platform: Platform,
     private splashScreen: SplashScreen,
     private statusBar: StatusBar,
     public authService: AuthenticationService) 
  {
    this.sideMenu();
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  sideMenu()
  {
    this.navigate =
    [
      {
        title : "Register",
        url   : "/register",
        //icon  : "document-text-outline"
      },
      {
        title : "Login",
        url   : "/login",
        //icon  : "enter-outline"
      }
    ]
  }

  logout() {
    this.authService.SignOut()
  }

}
