import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service'
import { ProductService } from '../product.service'
import { AuthenticationService } from "../authentication.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  password : String
  email : String

  constructor(
    public authService: AuthenticationService,
    public usersService : UsersService, public router: Router) { }

  ngOnInit() {
  }

  login() {
    this.authService.SignIn(this.email, this.password)
      .then((res) => {
        this.usersService.get_user_data(res.user.uid).subscribe(data => {
          this.usersService.set_user_ls({ ...data[0], email: this.email });
          this.router.navigate(['/']);
        })
      }).catch((error) => {
        window.alert(error.message)
      })
  }

}
