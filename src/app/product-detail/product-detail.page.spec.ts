import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { By } from '@angular/platform-browser';

import { ProductDetailPage } from './product-detail.page';
import { ProductService } from '../product.service'

describe('ProductDetailPage', () => {
  let component: ProductDetailPage;
  let fixture: ComponentFixture<ProductDetailPage>;
  let debugE1: DebugElement;
  let htmlE1: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [IonicModule]

    })
    .compileComponents();
  }));

  beforeEach(inject([ProductService], (productService: any) => {
    const productdata: any = {
      title : 'kuva',
      description : 'kuva',
      price : '42',
    };
    productService = productdata;
    fixture = TestBed.createComponent(ProductDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  afterEach(() => {
    component = null;
    fixture.destroy();
    debugE1 = null;
    htmlE1 = null;
  });

  it('should create product detail page', () => {
    expect(component).toBeTruthy();
  });

  it('should contain title on header', () => {
    debugE1 = fixture.debugElement.query(By.css('ion-card-title'));
    htmlE1 = debugE1.nativeElement;
    expect(htmlE1.textContent).toContain('kuva');
  });

  it('should have price on content', () => {
    debugE1 = fixture.debugElement.query(By.css('ion-item'));
    htmlE1 = debugE1.nativeElement;
    expect(htmlE1.textContent).toContain('42')
  });

  it('should have description on content', () => {
    debugE1 = fixture.debugElement.query(By.css('ion-card-content'));
    htmlE1 = debugE1.nativeElement;
    expect(htmlE1.textContent).toContain('kuva')
  });
});
