import { Component, OnInit } from '@angular/core';
import { Product } from '../product/product'
import { ProductService } from '../product.service'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  product : Product = new Product
  id : string 

  constructor(
    private productservice : ProductService,
    private route: ActivatedRoute
  ) {
    this.get_product()
   }

  ngOnInit() {
  }

  async get_product() {
    await this.route.params.subscribe(params => {
      console.log(params['id'])
      this.id = params['id']
      this.productservice.get_product(params['id']).subscribe(res => {
        this.product = res
      })
    });
  }

  delete_product() {
    console.log(this.id)
    this.productservice.del_product(this.id)
  }
}
