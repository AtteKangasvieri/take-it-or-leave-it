import { Injectable } from '@angular/core';
import { Product } from './product/product'
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore'
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { map, finalize } from "rxjs/operators";
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private users: Array<Product>
  UploadedFileURL
  fileName
  filepath

  constructor(
    private db: AngularFirestore,
    private storage: AngularFireStorage,
    public router: Router
    ) {

  }

  get_products(): Observable<any> {
    return this.db.collection<any>( "products" ).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Product
        const pid = a.payload.doc.id
        return { pid, ...data }
      })))
  }

  get_product(pid) {
    return this.db.doc(`products/${pid}`).snapshotChanges().pipe(
      map(actions => {
        const data = actions.payload.data() as Product
        const pid = actions.payload.id
        return { pid, ...data }
      }))
  }

  get_recently_added() {
    return this.db.collection( "products",ref => ref.orderBy('date','desc').limit(10) ).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Product
        const pid = a.payload.doc.id
        return { pid, ...data }
      })))
  }

  get_my_products(uid) {
    return this.db.collection<any>( "products",ref => ref.where('user_id','==',uid)).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Product
        const pid = a.payload.doc.id
        return { pid, ...data }
      })))
  }

  //Todo
  search() {
    return this.db.collection('products').snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Product
        const pid = a.payload.doc.id
        return { pid, ...data }
      })))
  }

  uploadFile(event: FileList) {
    // The File object
    alert('hi')
    const file = event.item(0)
    alert('hi')
    // Validation for Images Only
    if (file.type.split('/')[0] !== 'image') { 
     alert('unsupported file type :( ')
     return;
    }

    // The storage path
    const path = `Images/${new Date().getTime()}_${file.name}`;
 
    //File reference
    const fileRef = this.storage.ref(path);
 
    // The main task
    let task = this.storage.upload(path, file);
    

    return task.snapshotChanges().pipe(finalize(() => {
      // Get uploaded file storage path
      this.UploadedFileURL = fileRef.getDownloadURL();
      
      this.UploadedFileURL.subscribe(resp=>{
        return [file.name,resp]
      },error=>{
        console.error(error);
      })
    }))
  }

  async uploadPicture(imageString) {
    try {
      const storageRef = firebase
      .storage()
      .ref(`images/${new Date().getTime()/1000}`)
      const uploadedPicture = await storageRef.putString(imageString, 'data_url',{contentType:'image/jpeg'})
      return await storageRef.getDownloadURL();
    } catch (error) {
      alert(error.message)
    }
    
   }

  add_product(product : Product) {
    let user = JSON.parse(localStorage.getItem('user'))
    const productdata = {
      title : product.title,
      id : product.id,
      description : product.description,
      price : product.price,
      pictures : product.pictures,
      date : Date.now(),
      user_id : user.uid
    }
    return this.db.collection(`products`).add(productdata).then(data => {return data.id})
  }

  update_product(product) {
    product.title = "different"
    this.db.doc('products/' + product.pid).update({
      title : product.title,
      id : product.id,
      description : product.description,
      price : product.price,
      pictures : product.pictures,
      user_id : product.user_id
    })
  }

  /**
   * delete product
   * @param pid
   */
  del_product(pid : string) {
    this.get_product(pid).subscribe(res => {
      if(res.pictures) {
        this.storage.storage.refFromURL(res.pictures).delete()
      } 
      this.db.doc(`products/${pid}`).delete().then( res => {
        this.router.navigate([''])
      })
    })
    
  }

}
