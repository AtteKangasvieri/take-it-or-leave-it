import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { User } from '../user/user';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  email: string;
  password: string;
  firstname: string;
  lastname: string;
  address: string;
  phonenumber: string; 

  constructor(
    public navCtrl: NavController,
    public toastController: ToastController,
    public authService: AuthenticationService,
    public router: Router,
    public usersService : UsersService,
  ) { }

  ngOnInit() {}

  async presentToast() {
    let toast = await this.toastController.create({
      message: 'Your account was registered successfully',
      duration: 4000,
      position: 'top'
    });
  
    toast.present();
  }
//
  signUp() {
    let user : User = {
      uid : "",
      address : this.address,
      phonenumber : this.phonenumber,
      firstname : this.firstname,
      lastname : this.lastname,
      gdpr_accept: true
    }
    console.log(user)
    this.authService.RegisterUser(this.email, this.password)
      .then((res) => {
        user.uid = res.user.uid
        this.usersService.add_user(user)
        this.usersService.set_user_ls({ ...user, email: this.email })
        this.presentToast()
        this.router.navigate(['/tabs/tab3']);
      }).catch((error) => {
        window.alert(error.message)
      })
  }
}
