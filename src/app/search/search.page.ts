import { Component, OnInit } from '@angular/core';
import { User } from '../user/user'
import { Product } from '../product/product'
import { UsersService } from '../users.service'
import { ProductService } from '../product.service'
import { AuthenticationService } from "../authentication.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  products : Array<Product>
  searchTerm : string = ""

  constructor(
    public authService: AuthenticationService,
    public usersService : UsersService,
    public productservice : ProductService
    ) { 
      
    }

  ngOnInit() {
  }

  get_results() {
    this.productservice.search().subscribe(res => {
      let temp_products = res
      this.products = temp_products.filter(item => {
        if (item.title && this.searchTerm) {
          if (item.title.toLowerCase().match(this.searchTerm)) {
            return true;
          }
          return false;
        }
      })
      console.log(this.products)
    })
  }

}
