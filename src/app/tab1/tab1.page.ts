import { Component, Testability } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../user/user'
import { Product } from '../product/product'
import { UsersService } from '../users.service'
import { ProductService } from '../product.service'
import { AuthenticationService } from "../authentication.service";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  Users: Observable<User[]>;
  RecentlyAdded: any;
  user : User = {
    uid : "",
    address : "somewhere",
    //birthday : new Date,
    phonenumber : "",
    firstname : "test",
    lastname : "test",
    //emailVerified : false
    gdpr_accept: true
  }
  product : Product = {
    pid : '7TfGtlW2ac0YvP93l2p5',
    title : 'test',
    id : '',
    description : '',
    price : 0,
    pictures : '',
    date : 0,
    user_id : ''
  }

  constructor(
    public authService: AuthenticationService,
    public usersService : UsersService,
    public productservice : ProductService) {
    //this.get_users()
    //this.get_user('3PobB8sQRcHojVTqA4qu')
    //this.test_login()
    //this.test_register()
    //console.log(this.usersService.get_user_ls())
    //this.authService.SignOut()
    //this.productservice.update_product(this.product)
    //this.test_add_product()
    //this.test_get_recently_added()
    /*let uid = JSON.parse(localStorage.getItem('user'))
    uid = uid.uid
    console.log(uid)
    console.log(this.test_get_my_products(uid))*/
    //this.productservice.del_product('eceQlNMvOCdCQ8zijtZU')
  }

  ngOnInit(): void {
    this.usersService.get_users()
    .subscribe((res: any) => {
     this.Users = res;
     console.log(this.Users, 'jjdjdjdjdjdjd');
   }, err => {
     console.log(err);
   });

   this.productservice.get_recently_added()
   .subscribe((res: any) => {
    this.RecentlyAdded = res;
    console.log(this.RecentlyAdded, 'newwwww');
    }, err => {
      console.log(err);
    });
  }

  // /**
  //  * test function for getting user list
  //  */
  // get_users() {
  //   this.usersService.get_users().subscribe((data) => {
  //     this.users = data
  //     console.log(this.users, 'kkkkk')
  //   })
  // }

  // test_get_recently_added() {
  //   this.productservice.get_recently_added().subscribe((data) => {
  //     console.log(data)
  //   })
  // }

  test_get_my_products(uid) {
    this.productservice.get_my_products(uid).subscribe((data) => {
      console.log(data)
    })
  }

  test_register() {
    this.authService.RegisterUser("takeleave2022@gmail.com", "test6433i")
      .then((res) => {
        this.user.uid = res.user.uid
        this.usersService.add_user(this.user)
        this.usersService.set_user_ls(this.user)
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  test_login() {
    this.authService.SignIn("takeleave2022@gmail.com", "test6433i")
      .then((res) => {
        this.usersService.get_user_data(res.user.uid).subscribe(data => {
          this.usersService.set_user_ls(data[0])
        })
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  test_add_product() {
    this.productservice.add_product(this.product)
  }

}
