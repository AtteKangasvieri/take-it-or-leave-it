import { UsersService } from './../users.service';
import { RegisterPageModule } from './../register/register.module';
import { RegisterPage } from './../register/register.page';
import { ProductService } from './../product.service';
import { Product } from './../product/product';
import { AngularFirestore } from '@angular/fire/firestore';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './../user/user'

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  User : User | any;
  constructor(private router: Router, private userService: UsersService) {}

   ngOnInit(): void {
     this.User = this.userService.get_user_ls();
     if (!this.User) {
      this.router.navigate(['/register']);
     }
   }

  EditProfile(){
    this.router.navigate(['/edit-profile']);
  };

  MyProducts(){
    this.router.navigate(['/']);
  };

  //productPosts

 // constructor(private afs: AngularFirestore, private Product: ProductService) {
 //   const product = afs.doc('Product/${Product.getUID()}')
  //  this.productPosts = product.valueChanges()
  //}

}
