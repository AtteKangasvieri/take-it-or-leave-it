export interface User {
  uid : string
  firstname : string
  lastname : string
  address : string
  phonenumber : string
  //birthday : Date
  gdpr_accept: boolean
}