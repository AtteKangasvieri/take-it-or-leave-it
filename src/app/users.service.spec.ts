import { TestBed } from '@angular/core/testing';

import { UsersService } from './users.service';
import { User } from './user/user';

describe('UsersService', () => {
  beforeEach(() => 
  TestBed.configureTestingModule({
    
  }));

  it('user service should be created', () => {
    const service: UsersService = TestBed.get(UsersService);
    expect(service).toBeTruthy();
  });

  it('should contain current user', () => {
  let service: UsersService = TestBed.get(UsersService);
    const users : User = {
      uid : "",
      address : "somewhere",
      phonenumber : "",
      firstname : "test",
      lastname : "test",
      gdpr_accept: true
    }
    expect(typeof (users)).toEqual('object');
    expect(Object.keys(users)).toContain('firstname');
    expect(Object.keys(users)).toContain('lastname');
    expect(Object.keys(users)).toContain('address');
    expect(Object.keys(users)).toContain('phonenumber');
    expect(users.firstname).toContain('test');
    expect(users.lastname).toContain('test');
    expect(users.address).toContain('somewhere');
    expect(users.phonenumber).toContain('');
  });
});
