import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore'
import { User } from 'src/app/user/user'
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private users: Array<User>
  private user: User
  userData: any;

  constructor(
    private db: AngularFirestore
  ) {

  }

  /**
   * Get user list
   */
  get_users(): Observable<User[]> {
    return this.db.collection<any>( "users" ).valueChanges()
  }

  /**
   * Get single user by id
   * @param id 
   */
  get_user_data(uid : string) {
    return this.db.collection('users', ref => ref.where('uid', '==', uid)).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as User
        const uid = a.payload.doc.id
        return { uid, ...data }
      })))
  }

  /**
   * Adding new user to database
   * @param user 
   */
  add_user(user : User) {
    const userRef: AngularFirestoreDocument<any> = this.db.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      address: user.address,
      //birthday: user.birthday,
      //email: user.email,
      firstname: user.firstname,
      lastname: user.lastname,
      phonenumber: user.phonenumber,
      //emailVerified: user.emailVerified
      gdpr_accept: user.gdpr_accept
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  /**
   * Update user data
   */
  update_user(user) {
    this.db.doc('/users/' + user.uid).update({
      uid: user.uid,
      address: user.address,
      firstname: user.firstname,
      lastname: user.lastname,
      phonenumber: user.phonenumber,
      gdpr_accept: user.gdpr_accept
    }).then(res => {
      localStorage.setItem('user', JSON.stringify(user));
    })
  }

  /**
   * delete user
   * @param uid
   */
  del_user(uid : string) {
    this.db.doc('/users/' + uid).delete()
  }

  set_user_ls(user) {
    localStorage.setItem('user', JSON.stringify(user))
  }

  get_user_ls() {
    return JSON.parse(localStorage.getItem('user'));
    console.log(JSON.parse(localStorage.getItem('user')))
  }

}
