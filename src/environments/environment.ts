// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCMHnbJZYEmq069jP8wuliuuyNa8A-Hfmk",
    authDomain: "takeitorleaveit-94806.firebaseapp.com",
    databaseURL: "https://takeitorleaveit-94806.firebaseio.com",
    projectId: "takeitorleaveit-94806",
    storageBucket: "takeitorleaveit-94806.appspot.com",
    messagingSenderId: "802249942692",
    appId: "1:802249942692:web:9214e43c652f20deffe831"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
